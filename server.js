var express = require('express'),
app = express(),
cors = require('cors'),
bodyParser = require('body-parser'),
async = require('async'),
mkdirp = require('mkdirp'),
config = require('./config/config.js');

var appRoot = require('app-root-path');
var resemble = require('./resemble.js');
var fs = require('fs');
var gm = require('gm').subClass({imageMagick: true});
var cv = require('opencv/lib/opencv');
var COLOR = [0, 255, 0];

var applicationUrl = 'http://' + config.domain + ':' + config.port;
mkdirp.sync('uploads');
mkdirp.sync('markedImages');
mkdirp.sync('croppedImages');
mkdirp.sync('temp');

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Accept, Authorization');
	next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(cors());

app.use(express.static(__dirname+'/client'));
app.use('/uploads',express.static(__dirname+'/uploads'));
app.use('/croppedImages',express.static(__dirname+'/croppedImages'));
app.use('/markedImages',express.static(__dirname+'/markedImages'));
var multer = require('multer');
var uploads_destination = appRoot+"/uploads/";
var cropped_destination = appRoot+"/croppedImages/";
var marked_destination = appRoot+"/markedImages/";
var upload = multer({ 
	storage: multer.diskStorage({
	  destination: function (req, file, callback) {
	  		callback(null, uploads_destination);
	  },
	  filename: function (req, file, callback) {
	  	var ext = file.originalname.split('.')[1];
	    callback(null, file.fieldname + '_' + Date.now()+'.'+ext);
	  }
	})
});

app.get('/',function(req,res){
	res.sendFile(__dirname + "/client/facedetection.html");
});

var compareUpload = upload.fields([
        { name: 'image1', maxCount: 1 },
        { name: 'image2', maxCount: 1 }
    ]);

function imageCompare(img1,img2, cbmain){
	var file1=cropped_destination+img1;
	var file2=cropped_destination+img2;
	gm(file1)
	.resizeExact(48, 48)
	.write(appRoot+'/temp/r1.png', function (err) {
		if(err){
			cbmain(err, null);
		}else{
			gm(file2)
			.resizeExact(48, 48)
			.write(appRoot+'/temp/r2.png', function (err) {
				if(err){
					cbmain(err, null);
				}else{
					var fileData1 = fs.readFileSync(appRoot+'/temp/r1.png');
					var fileData2 = fs.readFileSync(appRoot+'/temp/r2.png');
					resemble(fileData1).compareTo(fileData2)
					.onComplete(function(data){
						fs.unlinkSync(appRoot+'/temp/r2.png');
						fs.unlinkSync(appRoot+'/temp/r1.png');
						cbmain(null, data);
					});
				}
			});
		}
	});
}

function detectFaceandCrop(image, cbmain){
	cv.readImage(uploads_destination+image, function(err, im) {
		if (err) cbmain(err, null);
		if (im.width() < 1 || im.height() < 1) throw new Error('Image has no size');
		im.detectObject('./haarcascade_frontalface_alt.xml', {}, function(err, faces) {
			if (err) cbmain(err, null);
			if(faces.length>0){
				face = faces[0];
				im.rectangle([face.x, face.y], [face.width, face.height], COLOR, 2);
				im.convertGrayscale();
				im.save(marked_destination+image);
				gm(marked_destination+image)
				.crop(face.width,face.height,face.x,face.y)
				.write(cropped_destination+image, function (err) {
					if (err) cbmain(err, null);
					cbmain(null, true);
				});
			}else{
				cbmain("No Face Detected", null);
			}
		});
	});	
}

app.post('/compare', compareUpload, function(req,res, next){
	var image1, image2;
	if(req.files.image1){
        image1 = req.files.image1[0].filename;
    }
    if(req.files.image2){
        image2 = req.files.image2[0].filename;
    }
    var iArray = [image1,image2];
    async.each(iArray, function(image, callback) {
	    detectFaceandCrop(image, function(err, result){
	    	if(err) callback(err);
	    	if(result) callback(null);
	    });
	},function(err){
		if(err){
			return res.status(500).send({success:false, error:err});
		}else{
			imageCompare(image1, image2, function(err, result){
		    	if(!err){
		    		return res.status(200).send({
				    	success:true,
				    	image1:image1,
				    	image2:image2, 
				    	result:result
				    });
		    	}else{
		    		return res.status(500).send({success:false, error:err});
		    	}
		    });
		}
	});
});

app.post('/facedetection', upload.single('image1'), function(req,res, next){
	if( req.file === undefined){
		return res.status(500).json({
		        success: false,
		        message: 'Image Required for Face Detection, Please Upload..!'
		});
	}
	if(req.file !== undefined ){
		var image = req.file.filename;
		cv.readImage(uploads_destination+image, function(err, im) {
			if (err) throw err;
			if (im.width() < 1 || im.height() < 1) throw new Error('Image has no size');
			im.detectObject('./haarcascade_frontalface_alt.xml', {}, function(err, faces) {
				if (err) throw err;
				if(faces.length>0){
					face = faces[0];
					im.rectangle([face.x, face.y], [face.width, face.height], COLOR, 2);
					im.convertGrayscale();
					im.save(marked_destination+image);
					gm(marked_destination+image)
					.crop(face.width,face.height,face.x,face.y)
					.write(cropped_destination+image, function (err) {
						if (err) return res.status(500).send({success:false, error:err});
						return res.status(200).send({
							success:true, 
							filename:image
						});
					});
				}else{
					return res.status(200).send({
						success:false,
						filename:image, 
						message:"No Face Detected in the Picture"
					});
				}
			});
		});		
	}else{
		return res.status(500).send({success:false, error:"File is not uploaded..!"});
	}
});


process.on('uncaughtException', function(err) {
    console.log("uncaughtException:==>"+err)
});

// UNKNOWN ROUTES
app.use('*',function(req, res) {
	res.status(404).json({error:"requested url: "+req.baseUrl+ ' is not Found'});
});

// START THE SERVER
app.listen(config.port, function(){
	console.log('Server is running at ==> ' + applicationUrl);    
});
