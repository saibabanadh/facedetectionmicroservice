# FACE DETECTION using OPENCV

## It is a Micro Service on Node Platform

### Steps

* User can send an Image to the API
* Service will ask OpenCV to detect the Face from Image
* Service with help of GraphicImage Library, crop the face from image
* Service will send that cropped image to user

