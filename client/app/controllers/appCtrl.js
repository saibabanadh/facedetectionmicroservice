var app = angular.module('facedetectionDemoApp',['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{
		controller:'facedetectionCtrl',
		templateUrl:'./app/partials/_facedetection.html'
	})
    .when('/compare',{
        controller:'compareCtrl',
        templateUrl:'./app/partials/_compare.html'
    })
    .otherwise({
        redirectTo:'/'
    });
});

app.directive('fileUpload', function () {
    return {
        scope: true,        
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                if(event.target.files.length){
                    scope.$emit("fileSelected", { files: files });
                }else{
                    scope.$emit("fileSelected", { file: {name:"No File Selected"} });
                }
            });
        }
    };
});

app.controller('facedetectionCtrl',function($scope, $http){
    $('#message-div').hide();
	$scope.files=[];
    $scope.original_image='../../public/images/200x150.png';
    $scope.marked_image='../../public/images/200x150.png';
    $scope.cropped_image='../../public/images/200x150.png';

	$scope.$on("fileSelected", function (event, args) {
        $('#message-div').hide();
        $scope.$apply(function () {
            console.log(args.files[0]);
            for(var i=0; i<args.files.length;i++){
                $scope.files.push(args.files[i]);  
            }
        });
    });

	$scope.uploadImage = function(){
        console.log($scope.files);
        $('#message-div').hide();
        $scope.original_image='../../public/images/200x150.png';
        $scope.marked_image='../../public/images/200x150.png';
        $scope.cropped_image='../../public/images/200x150.png';
		$('#mydiv').show();
  		$http({
            method: 'POST',
            url: "/facedetection",
            headers: { 'Content-Type': undefined },
            data: { 
            	image1: $scope.files[0] 
            },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("image1", data.image1);
                console.log(formData);
                return formData;
            }
        }).
        success(function (data, status, headers, config) {
            console.log(data);
            $('#mydiv').hide();
            $scope.original_image='/uploads/'+data.filename;
            if(data.success){
                $('#message-div').show();
                $scope.marked_image='/markedImages/'+data.filename;
                $scope.cropped_image='/croppedImages/'+data.filename;
            }else{
                alert(data.message);
            }
        }).
        error(function (error, status, headers, config) {
            console.log(error);
            $('#mydiv').hide();
        });
	}
}); // facedetection controller end




app.controller('compareCtrl',function($scope, $http){
    $('#message-div').hide();
    $scope.files=[];
    $scope.matching_percentage='Result';
    $scope.cropped_image1='../../public/images/200x150.png';
    $scope.cropped_image2='../../public/images/200x150.png';
    $scope.$on("fileSelected", function (event, args) {
        $('#message-div').hide();
        $scope.$apply(function () {
            for(var i=0; i<args.files.length;i++){
                $scope.files.push(args.files[i]);  
            }
        });
    });

    $scope.uploadImages = function(){
        console.log($scope.files)
        $('#message-div').hide();
        $('#mydiv').show();
        $http({
            method: 'POST',
            url: "/compare",
            headers: { 'Content-Type': undefined },
            data: { 
                image1: $scope.files[0],
                image2: $scope.files[1] 
            },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("image1", data.image1);
                formData.append("image2", data.image2);
                return formData;
            }
        }).
        success(function (data, status, headers, config) {
            console.log(data);
            $('#mydiv').hide();
            $scope.matching_percentage = (100 - data.result.misMatchPercentage).toFixed(2)+'%';
        }).
        error(function (error, status, headers, config) {
            console.log(error);
            $('#mydiv').hide();
        });
    }
}); // compare controller end